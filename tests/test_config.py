import subprocess


def test_clean_up():
    subprocess.check_call(["sh", "tests/data/check.sh"])


def test_logging():
    with open("clean_app.py") as f:
        assert "logging" in f.read(), "Please add logging"
