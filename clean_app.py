import shutil
import os
import tempfile
import sys
import logging

logging.basicConfig(level=logging.INFO)
def find_a_name_in_list(list, pattern):
    print(list)
    to_delete = True
    for f in list:
        if f == pattern:
            to_delete = False
    return to_delete

def main():
    filename = sys.argv[1]
    filename_base = filename.split("/")[-1].split("\\")[-1].split(".")[0]
    if os.path.exists(filename):
        with tempfile.TemporaryDirectory() as tmpdir:
            try:
                shutil.unpack_archive(filename, tmpdir)
                logging.info(f"Temp directory path: {tmpdir}")
                deleted_folders = []
                for dirpath, dirnames, files in os.walk(f"{tmpdir}"):
                    if dirpath == tmpdir:
                        logging.info("Root directory, skipping")
                        continue
                    else:
                        if find_a_name_in_list(os.listdir(dirpath), "__init__.py"):
                            folder_name = dirpath.split(tmpdir)[-1][1:]
                            deleted_folders.append(folder_name)
                            logging.warning(f"Going to delete a folder {folder_name} "
                                            f"since it does not contain the __init__.py file")
                            shutil.rmtree(dirpath)
                with open(f"{tmpdir}/cleaned.txt", "w") as log_file:
                    log_file.write("\n".join(sorted(deleted_folders)))
                    if deleted_folders:
                        log_file.write("\n")
                logging.info(os.listdir(tmpdir))
                shutil.make_archive(f"{filename_base}_new", "zip", f"{tmpdir}")
            except Exception as e:
                logging.warning("Something has gone wrong with the file you've provided")
                logging.error(f"ERROR: {e}")
    else:
        logging.error(f"The provided file {filename} does not exist.")

if __name__ == "__main__":
    main()
